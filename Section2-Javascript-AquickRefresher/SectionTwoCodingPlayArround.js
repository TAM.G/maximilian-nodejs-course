//to create Dir => mkdir directoryname
// to create file => echo filename.fileextention or touch filename.fileextention (touch is better and works in all conditions)


//#region  Try Spread Operator

    const car = {
        mark : 'Kia Serato',
        color : 'Grey',
        GetDriver (){
            console.log('Hey, Iam Driver');
        }
    };
    console.log(car);
    const carReplica = {...car};
    console.log(carReplica); // { mark: 'Kia Serato', color: 'Grey', GetDriver: [Function: GetDriver] }
    const carArr = ['name', 'color', 'Hello'];
    console.log(carArr);//[ 'name', 'color', 'Hello' ]
    const carrarrreplica = {...carArr};
    console.log(carrarrreplica); //{ '0': 'name', '1': 'color', '2': 'Hello' }

//#endregion

//#region Try Rest Operator

    const ToArray = (...arguments) => {
        return arguments;
    }
    console.log(ToArray('hello', 'from', 'the other Side', 3 , 2 )); //[ 'hello', 'from', 'the other Side', 3, 2 ]
    console.log(ToArray('hello', 'from', 'the other Side', 3 , 2 ).toString());//hello,from,the other Side,3,2

//#endregion

//#region TRy Destructuring

    console.log(car.GetDriver()); //Hey, Iam Driver => irrelevant to topic just for testing purpose

    let {mark, GetDriver} = car;
    console.log(mark, GetDriver); //Kia Serato [Function: GetDriver]

    const hobbies = ['tennis', 'also tennis', 'i love tennis'];
    let [hobby1, hobby2] = hobbies;
    console.log(hobby1, hobby2); //tennis also tennis
    let [anotherhobby] = hobbies;
    console.log(anotherhobby); //tennis

//#endregion

//#region Try Async 

 const TrialFunctionMethodOne = (callbackm1) => {
    setTimeout(() =>{
        callbackm1('Here is a callback Method 1');
    } , 2000);
 }

 setTimeout(() => {
    TrialFunctionMethodOne(text => {
         console.log(text);
     });
 }, 2000);

 console.log('Hello here is 1');

 //Hello here => this came first
 //Here is a callback => this cam after 2seconds

///////////////////////////////////////////////////////////////////////////////////////

    const TrialFunctionMethodTwo = (callbackm2) => //console.log(callback('Hello from call back wirh no delay (no settimeout)'));
    {
    setTimeout(() =>{
        callbackm2(console.log('Here is a callback Method 2'));
    } , 2000);
    }

    setTimeout(() => {
        TrialFunctionMethodTwo(text => {
            return text; 
        });
    }, 2000);

    console.log('Hello here is 2');

//////////////////////////////////////////////////////////////////////////////////////////////

    const TrialFunctionMethodThree = (callback) => console.log(callback('Hello from call back with no delay (no settimeout) , Method 3'));
    setTimeout(() => {
        TrialFunctionMethodThree(text => {
            return text; 
        });
    }, 2000);

    console.log('Hello here is 3');


    /**
     * Results : 
        Hello here is 1
        Hello here is 2
        Hello here is 3
        Hello from call back with no delay (no settimeout) , Method 3
        Here is a callback Method 1
        Here is a callback Method 2
     */
//#endregion


//#region Try Promises => alternative for it is "async await"

 const PromiseTrial = () => {
     const promise = new Promise((resolve, reject) => {
         setTimeout(() => {
             resolve('ok, here is a promis :"D');
         }, 2000);

     });
     return promise;
 }
  
 const PromiseTwo = () => {
     const promise = new Promise((resolve, reject) => {
        resolve('Hello, this is another promise');
     });
     return promise
 }
 setTimeout(() => {
     PromiseTrial().then(text => {
         console.log(text);
         return PromiseTrial();
     }).then(text2 => {
         console.log(text2);
     });
 }, 2000);

//ok, here is a promis :"D => after 4seconds
//ok, here is a promis :"D => after 6seconds

setTimeout(() => {
    PromiseTrial().then(text => {
        console.log(text);
        return PromiseTwo();
    }).then(text2 => {
        console.log(text2);
    });
}, 2000);

//Hello, this is another promise
//ok, here is a promis :"D

//#endregion


//#region Trying Template literals (Template strings) => using backticks => backtick (` `) 

const name = 'Tasneem';
const country = 'Egypt';
console.log(`This is ${name}'s Greetings From ${country}`); //his is Tasneem's Greetings From Egypt
//#endregion