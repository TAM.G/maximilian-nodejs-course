const http = require('http');

//#region 
/*const server = http.createServer((req, res)=>{
  //console.log(req.url, req.method, req.headers);
    //console.log('alloha there');
  //  process.exit(); // un register event and that will shut the server down :D
//#region 
 /* 
 res.setHeader('Content-Type', 'text/html');
  res.write('<html>');
  res.write('<head> <title> Hello, its me </title> </head>');
  res.write('<body> <h1> Hello from the Other SIIIDE </h1> </body>');
  res.write('</html>');
  res.end();
  *** /
//#endregion
});
*/
//#endregion

const route = require('./routes');

//#Method #1
const server = http.createServer(route);

//Method #2
//const server = http.createServer(route.handler);

server.listen(3000);
