
const fs = require('fs');

const exportedFunction = (req, res) => 
{
        //console.log(req.url, req.method, req.headers);
        console.log('alloha there');
        //  process.exit(); // un register event and that will shut the aerver down :D
          const method = req.method;
        if(req.url === '/'){
          res.setHeader('Content-Type', 'text/html');
          res.write('<html>');
          res.write('<head> <title> Hello, its me </title> </head>');
          res.write('<body> <form action = "/message" method="POST"><input type = "text" name = "msg"><button type = "submit"> Send</button></form> </body>');
          res.write('</html>');
          return res.end();
        }
        /**
         * 1-in Video No 34 about how body and request is being recieved to the server, the raw idea of nodejs is to use buffers and streams
         * 2-so to get the whole request bosy you have to push the body to some sort of array to make get it all
         * 3-and to work with this parsed body request you get, you need to use Buufer and concat it 
         * 4-after that you can get the body you need and work on it
         * 
         *  */
        if(req.url ==='/message' && method === 'POST'){
          const body = [];
          req.on('data', (parsedBodyParts) => {
            console.log(parsedBodyParts);
              body.push(parsedBodyParts); //step #2
              //console.log(body);
          });
         return  req.on('end', () =>{
            const parsedBody = Buffer.concat(body).toString(); //step #3
            console.log('here is the whole body after req end', parsedBody);
            const recievedMsg = parsedBody.split('=')[1]; //step #4
            fs.writeFile('msg.txt', recievedMsg, () => {
              res.statusCode = 302;
              res.setHeader('Location', '/tasneem');
              return res.end();
            });
          
          });
        }
      
        if(req.url ==='/tasneem'){
        res.setHeader('Content-Type', 'text/html');
        res.write('<html>');
        res.write('<head> <title> Hello, its me </title> </head>');
        res.write('<body> <h1> Hello from the Other SIIIDE </h1> </body>');
        res.write('</html>');
        res.end();
      }
}

//Method #1
module.exports = exportedFunction;

//#method #2
// module.exports = {
//     handler : exportedFunction,
//     msg : 'some msg heres'
// }

//Method #3
//exports.handler = exportedFunction;