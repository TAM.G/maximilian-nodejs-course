const express= require('express');
const path = require('path');
const router = express.Router();
const adminActions = require('../routes/admin');

const rootDirectory = require('../utils/root-path');

    router.get('/',(req, res, next) =>{
        // console.log(" this middleware always runs sir!");
        //res.send('<h1> Hello from express!</h1>');
       // next();

    // //res.sendFile(path.join(__dirname,'../', 'views', 'shop.html'));
        //__dirname global variable that hold the absolute path on the operating system to the working space you are working on
        // the ../ means up one level and that is to reach the shop.html file as shopper.js file here is in a sibling folder(routes folder), u also can type it '..' instead of '../' and it still work thanks to path.join function
    // //         console.log("adminActions.Products", adminActions.Products);
    // //    res.sendFile(path.join(rootDirectory, 'views', 'shop.html'));

    /**
     * cuz we now are using engine templating so we are not returning html file we are retrening template
     * 
     */
        const products = adminActions.Products;
        // console.log(products);
        res.render('shop', {prods: products, pageTitle : 'Shop', 
        path : '/products',
        productsExist : products.length > 0,
       isProductsPage : true});

    
    });
 

module.exports = router;  