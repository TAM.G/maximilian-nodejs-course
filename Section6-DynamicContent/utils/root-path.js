const path = require('path');

module.exports =  path.dirname(require.main.filename);
 // this => "require.main.filename" gives us the path to the file resposible for the app to start which is app.js