const express = require('express');
const path = require('path');
const app = express();


app.use(express.urlencoded({extended : true}));
app.use(express.static(path.join(__dirname, 'shared')));

const userRoutes = require('./routes/user-routes');

app.use(userRoutes);


app.listen(3000);