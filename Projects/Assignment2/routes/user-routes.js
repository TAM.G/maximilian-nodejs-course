const express = require('express');
const path   = require('path');
const router = require('../../Section5-WorkingWithExpress/routes/admin');

const routing = express.Router();

const rootDir = require('../shared/utils/rootPath');

routing.get('/', (req, res) =>{
     res.sendFile(path.join(rootDir, 'views', 'add-user.html'));
});

routing.get('/users',(req, res) =>{
          // res.redirect('/users');
          res.sendFile(path.join(rootDir, 'views', 'users.html'));
} );

module.exports= routing;

