const express = require('express');
const authUser = require('./auth-user');

const router = express.Router();

router.get('/users', (req, res)=>{
    const users = authUser.users;
    console.log("USERSSS", users);
        res.render('users', {pageTitle : 'Users', users : users});
});

module.exports = router;