const express = require('express');
const router = express.Router();

const users = [];

router.get('/',(req, res)=> {
    res.render('add-user', {pageTitle : 'Add User'});
})

router.post('/users', (req,res)=>{

    console.log("req.body", req.body);
    users.push(req.body);

    res.redirect('/users');
});


//module.exports = router;

exports.router = router;
exports.users = users;