const express = require('express');
const app = express();
app.use(express.urlencoded({extended : true}));
app.set('view engine', 'ejs');
app.set('views', 'views');

const authUserRoutes = require('./routes/auth-user');
const usersRoutes = require('./routes/users');

app.use(authUserRoutes.router);
app.use(usersRoutes);

app.listen(5000);