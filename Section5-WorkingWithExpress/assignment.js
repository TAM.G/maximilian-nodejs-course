const express = require('express');

const app = express();

app.use('/enrolled-course',(req, res, next)=>{

console.log('This is first routing that will go to the second one for execution');
next();
});

app.use('/alloha-there', (req, res)=>{
res.send('Hello from the other siiide');
});

app.use('/users', (req, res, next)=>{
res.send('Hello, my name is Tasneem');
});

app.use('/', (req, res)=>{
res.send('Hello, this is the last middleware');
});

app.listen(3000);