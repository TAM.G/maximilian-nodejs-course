const express= require('express');
const path = require('path');
const router = express.Router();


const rootDirectory = require('../utils/root-path');

    router.get('/',(req, res, next) =>{
        // console.log(" this middleware always runs sir!");
        //res.send('<h1> Hello from express!</h1>');
       // next();

    // //res.sendFile(path.join(__dirname,'../', 'views', 'shop.html'));
        //__dirname global variable that hold the absolute path on the operating system to the working space you are working on
        // the ../ means up one level and that is to reach the shop.html file as shopper.js file here is in a sibling folder(routes folder), u also can type it '..' instead of '../' and it still work thanks to path.join function
    
       res.sendFile(path.join(rootDirectory, 'views', 'shop.html'));

    
    });
 

module.exports = router;  