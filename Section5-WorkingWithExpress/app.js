
const express = require('express');
const path = require('path');
const app = express(); 
app.use(express.urlencoded({ extended: true }))

/* hena 3lashan ne2dar n serve files statically la2en 7etet 2nna n7awel nesta5dem files zay styling aw images, 
express betemna3 da, fa benesta5dem express.static 3lshan ne access el files de
*/
app.use(express.static(path.join(__dirname, 'public')))

const adminRoutes = require('./routes/admin');
const shopperRoutes = require('./routes/shopper');
//app.use(adminRoutes, shopperRoutes);

//if am gonna use route filtration like the code below so, the line above won't work with filtering before routes
app.use('/admin',adminRoutes);
app.use('/shopper',shopperRoutes);

app.use((req, res, next)=>{
    //res.status(404).send('<h1>Page No Fooound</h1>');
    //res.sendFile(path.join(__dirname, 'views', '404.html'));
    res.status(404).sendFile(path.join(__dirname, 'views', '404.html'));
});

app.listen(3000);