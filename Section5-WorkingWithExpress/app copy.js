//const http = require('http'); // no need to export it if u using app.listen() ==> see app.listen function below
const express = require('express');

const app = express(); 


app.use(express.urlencoded({ extended: true }))




/**
 *  we did write expess() as a function cuz express exports itself as a function ==> (see express code)
 * declare function e(): core.Express; ==> line 27 in code
 * export = e; ==> last line in code
 */

 //when writing a function inside app.use(), that means it'll be executed before any coming request =>
 // this maybe useful if i needed to check authentication or anything else before passing the request
/*app.use((req, res, next) =>{
 console.log("Hello from first middleware");
 next();
});*/

// // app.use('/',(req, res, next) =>{
// //     console.log(" this middleware always runs sir!");
// //     //res.send('<h1> Hello from express!</h1>');
// //     next();
// //    });

app.use('/add-products',(req, res, next) =>{
 res.send('<form action ="/product" method="POST"><input type ="text" name="Proudct"><button type="submit">Add Product</button></form>');
})

app.post('/product', (req, res, next) =>{
    console.log(req.body);
    res.redirect('/');
})


app.use('/',(req, res, next) =>{
   // console.log(" this middleware always runs sir!");
    res.send('<h1> Hello from express!</h1>');
    next();
   });


/*const server = http.createServer(app);
server.listen(3000);*/

//this line works alrenatively for the 2lines above together. ==> see express documetation.
app.listen(3000);